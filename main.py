import triad_openvr
import time
import sys
import math

def degrees_to_radians(degrees):
    radians = []
    for degree in degrees:
        radians.append(math.radians(degree))
    return radians

def radians_to_degrees(radians):
    degrees = []
    for radian in radians:
        degrees.append(math.degrees(radian))
    return degrees